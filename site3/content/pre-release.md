---
title: Pre-releases
description: Specify which branches produce regular releases, and which pre-releases
weight: 33
---

# Pre-release

> **Warning!** Pre-release is an experimental feature. The behaviour is likely to change.

Pre-releases can be created when branches are divided to two groups by specifying
`GSG_RELEASE_BRANCHES` environment variable, or the corresponding command line option `--release-branches`.

`GSG_RELEASE_BRANCHES` takes a list of branch names, including wildcard support. Those branches will produce basic semantic release versions,
like `v1.2.3`. All other branches will produce pre-release versions, eg. `v2.0.0-alpha.1`.

The wildcard pattern matching algorithm is does not consider the branch path as a flat namespace, e.g. `release/*` does not include `release/*/v1.0`.

## Customize version scheme

The form of the pre-release can be customized by specifying templates for the 
[pre-release version](https://semver.org/spec/v2.0.0.html#spec-item-9) and 
[build metadata](https://semver.org/spec/v2.0.0.html#spec-item-10). Both options accept comma separated lists of 
identifier templates:

 Option | CLI flag | Environment variable
-------|----------|----------------------
pre-release template | `--pre-tmpl` | `GSG_PRE_TMPL` | `{{ env "CI_COMMIT_REF_SLUG" }},{{ seq }}`
build metadata template | `--build-tmpl` | `GSG_BUILD_TMPL` | `{{ env "CI_COMMIT_SHA" }}`

The default value for `GSG_PRE_TMPL` is 
```
{{ (env "CI_COMMIT_REF_SLUG") }},{{ seq }}
```
and it will produce versions
of form `v1.2.3-branchname.1`

### Templates

Templates are expanded using [go text/template](https://golang.org/pkg/text/template/).

Available functions:

Function | Description
---|---
`func env(string) string` | return value of environment variable
`func commitTS() time.Time` | time of the commit being released
`func seq() (string, error)` | generate a sequence number (not available for build metadata) 
